'use strict';
const db = uniCloud.database()
const role = 0
exports.main = async (event, context) => {
	let { res, user } = await cheak(event)
	if (res.code <= 1111) {
		const delete_location = await db.collection('location').where({
			_id: event.data._id
		}).remove()
		res.data = delete_location.data
	}
	return res
}

async function cheak(e){
	var res = { "code":"0000", "data":{} }
	var user = await db.collection('user').where({
		'_id'	:	e.uid,
		'token'	:	e.token
	}).get()
	user = user.data[0];
	if(user){
		if(role != user.role){
			res.code  = 4003
		}
	}else{
		res.code  = 4001;
	}
	return {res,user}
}